//
//  ResponseObject.swift
//  umwerk-assignment
//
//  Created by Miha Perne on 22/04/2019.
//  Copyright © 2019 Miha Perne. All rights reserved.
//

import Foundation

struct ResponseObject: Codable {
	
	let message: String?
	let totalCount: Int?
	let incompleteResults: Bool?
	let items: [GitHubUser]

}
