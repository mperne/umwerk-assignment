//
//  GitHubUser.swift
//  umwerk-assignment
//
//  Created by Miha Perne on 22/04/2019.
//  Copyright © 2019 Miha Perne. All rights reserved.
//

import Foundation

struct GitHubUser: Codable, Equatable {

	let login: String
	let id: Int
	let url: URL
	let avatarUrl: URL
	let htmlUrl: URL
	let followers: Int?
	let email: String?

	static func ==(lhs: GitHubUser, rhs: GitHubUser) -> Bool {
		return lhs.id == rhs.id
	}

}
