//
//  Storage.swift
//  umwerk-assignment
//
//  Created by Miha Perne on 22/04/2019.
//  Copyright © 2019 Miha Perne. All rights reserved.
//

import Foundation

class StorageManager {

	private let storageKey = "gitHubUsers"
	public static let shared = StorageManager()

	var userDefaults = UserDefaults(suiteName: "group.umwerk-assignment")

	public var gitHubUsers = [GitHubUser]() {
		didSet {
			save()
		}
	}

	init() {
		load()
	}

	func save() {
		userDefaults?.set(try? PropertyListEncoder().encode(gitHubUsers), forKey: storageKey)
		userDefaults?.synchronize()
	}

	func load() {
		guard let data = userDefaults?.object(forKey: storageKey) as? Data,
			let gitHubUsers = try? PropertyListDecoder().decode(Array<GitHubUser>.self, from: data) else { return }
		self.gitHubUsers = gitHubUsers
		userDefaults?.synchronize()
	}
}
