//
//  GitHubUsersViewModel.swift
//  umwerk-assignment
//
//  Created by Miha Perne on 22/04/2019.
//  Copyright © 2019 Miha Perne. All rights reserved.
//

import Foundation

class GitHubUsersViewModel {

	var gitHubUsers = [GitHubUser]()
	private let networkManager = NetworkManager()
	private let storageManager = StorageManager.shared

	private var pagesCached: Int {
		return Int(ceil(Float(gitHubUsers.count) / Float(Constants.Pagination.itemsPerPage)))
	}

	var isFetchInProgress = false

	var usersUpdateCallback: (([GitHubUser]) -> Void)?
	var usersResetCallback: (() -> Void)?
	var errorCallback: ((Error) -> ())?

	init() {
		fetchInitialUserData()
	}

	func fetchInitialUserData() {

		fetchUserData(fromPage: 1) { [unowned self] users in
			do {
				let newUsers = try users.get()
				self.gitHubUsers = newUsers
				self.storageManager.gitHubUsers = newUsers
				DispatchQueue.main.async {
					self.usersResetCallback?()
				}
			}
			catch {
				DispatchQueue.main.async {
					self.errorCallback?(error)
				}
			}
		}
	}

	func fetchNextBatch() {

		fetchUserData(fromPage: pagesCached + 1) { [unowned self] users in
			do {
				let newUsers = try users.get()
				self.gitHubUsers.append(contentsOf: newUsers)
				self.storageManager.gitHubUsers.append(contentsOf: newUsers)

				DispatchQueue.main.async {
					self.usersUpdateCallback?(newUsers)
				}
			}
			catch {
				DispatchQueue.main.async {
					self.errorCallback?(error)
				}
			}
		}

	}

	private func fetchUserData(fromPage page: Int, completion: @escaping (Result<[GitHubUser], Error>) -> Void) {

		guard !isFetchInProgress else { return }

		isFetchInProgress = true

		networkManager.dataTask(NetworkRequest.getUsers(page: page)) { [weak self] (result: Result<ResponseObject, Error>) in
			switch result {

			case .success(let responseObject):
				DispatchQueue.main.async {
					self?.isFetchInProgress = false
					completion(.success(responseObject.items))
				}

			case .failure(let error):
				DispatchQueue.main.async {
					self?.isFetchInProgress = false
					completion(.failure(error))
				}
			}
		}
	}
}
