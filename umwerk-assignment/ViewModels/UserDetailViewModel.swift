//
//  UserDetailViewModel.swift
//  umwerk-assignment
//
//  Created by Miha Perne on 22/04/2019.
//  Copyright © 2019 Miha Perne. All rights reserved.
//

import Foundation

class UserDetailViewModel {

	var gitHubUser: GitHubUser?
	private let networkManager = NetworkManager()

	private var isFetchInProgress = false

	func getUser(_ userName: String, completion: @escaping (Result<GitHubUser, Error>) -> Void) {

		networkManager.dataTask(NetworkRequest.getUser(userName: userName)) { (result: Result<GitHubUser, Error>) in
			do {
				let user = try result.get()
				completion(.success(user))
			}
			catch {
				completion(.failure(error))
			}
		}
	}
}
