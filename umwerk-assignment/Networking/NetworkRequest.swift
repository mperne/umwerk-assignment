//
//  NetworkRequest.swift
//  umwerk-assignment
//
//  Created by Miha Perne on 22/04/2019.
//  Copyright © 2019 Miha Perne. All rights reserved.
//

import Foundation

public enum HTTPMethod : String {
	case get     = "GET"
	case post    = "POST"
	case put     = "PUT"
	case patch   = "PATCH"
	case delete  = "DELETE"
}

enum NetworkError: String, Error {
	case data = "Data is nil"
	case request = "URL Request is nil"
}

enum NetworkRequest {

	case getUsers(page: Int)
	case getUser(userName: String)

	private var method: HTTPMethod {
		switch self {
		case .getUsers(_),
			 .getUser(_):
			return .get
		}
	}

	private var host: String {
		switch self {
		case .getUsers(_),
			 .getUser(_):
			return Constants.Network.host
		}
	}

	private var scheme: String {
		switch self {
		case .getUsers(_),
			 .getUser(_):
			return Constants.Network.scheme
		}
	}

	private var path: String {
		switch self {
		case .getUsers(_):
			return Constants.Network.usersPath
		case .getUser(let userName):
			return Constants.Network.userPath.appending("/\(userName)")
		}
	}

	private var parameters: [URLQueryItem] {

		switch self {

		case .getUsers(let page):

			return [URLQueryItem(name: "q", value: "type:user+language:java"),
					URLQueryItem(name: "page", value: String(page)),
					URLQueryItem(name: "per_page", value: String(Constants.Pagination.itemsPerPage))]

		case .getUser(_):

			return []

		}
	}

	var urlRequest: URLRequest? {
		var components = URLComponents(string: Constants.Network.host)!
		components.scheme = scheme
		components.host = host
		components.path = path
		components.queryItems = parameters

		guard let url = components.url else { return nil }

		var urlRequest = URLRequest(url: url)
		urlRequest.httpMethod = method.rawValue

		return urlRequest
	}
}
