//
//  NetworkManager.swift
//  umwerk-assignment
//
//  Created by Miha Perne on 22/04/2019.
//  Copyright © 2019 Miha Perne. All rights reserved.
//

import Foundation

class NetworkManager {

	func dataTask<T: Codable>(_ request: NetworkRequest, completion: @escaping (Result<T, Error>) -> Void) {

		guard let urlRequest = request.urlRequest else {
			completion(.failure(NetworkError.request))
			return
		}

		URLSession.shared.dataTask(with: urlRequest) { data, response, error in
			if let error = error {
				completion(.failure(error))
				return
			}

			guard let data = data else {
				completion(.failure(NetworkError.data))
				return
			}

			do {
				let responseObject = try JSONDecoder().gitHubResponseDecoder.decode(T.self, from: data)
				completion(.success(responseObject))
			}
			catch let decodeError {
				completion(.failure(decodeError))
			}

			}.resume()
	}

}
