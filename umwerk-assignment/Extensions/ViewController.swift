//
//  ViewController.swift
//  umwerk-assignment
//
//  Created by Miha Perne on 22/04/2019.
//  Copyright © 2019 Miha Perne. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {

	func performSegue(with identifier: Segue, sender: Any?) {
		performSegue(withIdentifier: identifier.rawValue, sender: sender)
	}

}
