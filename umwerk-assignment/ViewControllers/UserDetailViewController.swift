//
//  UserDetailViewController.swift
//  umwerk-assignment
//
//  Created by Miha Perne on 22/04/2019.
//  Copyright © 2019 Miha Perne. All rights reserved.
//

import Foundation
import UIKit
import Nuke
import SafariServices

class UserDetailViewController: UIViewController {
	
	@IBOutlet private weak var userImageView: UIImageView!
	@IBOutlet private weak var userNameLabel: UILabel!
	@IBOutlet private weak var emailLabel: UILabel!
	@IBOutlet private weak var websiteLabel: UILabel!
	@IBOutlet private weak var followerCountLabel: UILabel!

	var user: GitHubUser?
	var userViewModel = UserDetailViewModel()

	override func viewDidLoad() {
		super.viewDidLoad()
		guard let user = user else { return }

		userViewModel.getUser(user.login) { [unowned self] (result) in
			do {
				let user = try result.get()
				DispatchQueue.main.async {
					self.user = user
					Nuke.loadImage(with: user.avatarUrl, into: self.userImageView)
					self.userNameLabel.text = user.login
					self.emailLabel.text = user.email

					self.websiteLabel.text = user.htmlUrl.absoluteString

					self.followerCountLabel.text = "Followers: " + String(user.followers ?? 0)
					self.emailLabel.isHidden = user.email == nil

					self.addLinkTapGestureRecognizer()
					self.addEmailTapGestureRecognizerIfNeeded()
				}
			}
			catch {
				print(error)
			}
		}
	}

	func addLinkTapGestureRecognizer() {
		guard let urlString = user?.htmlUrl.absoluteString  else { return }

		websiteLabel.attributedText = createaUnderlinedAttributedString(from: urlString)
		appyTapGestureRecognizer(to: websiteLabel, with: #selector(openLink))

	}

	func addEmailTapGestureRecognizerIfNeeded() {
		guard let userEmail = user?.email else { return }

		emailLabel.attributedText = createaUnderlinedAttributedString(from: userEmail)
		appyTapGestureRecognizer(to: emailLabel, with: #selector(openEmailApp))

	}

	func createaUnderlinedAttributedString(from text: String) -> NSAttributedString {
		return NSAttributedString(string: text, attributes:
			[.underlineStyle: NSUnderlineStyle.single.rawValue])
	}

	func appyTapGestureRecognizer(to view: UIView, with selector: Selector) {
		let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: selector)
		view.isUserInteractionEnabled = true
		view.addGestureRecognizer(tapGestureRecognizer)
	}

	@objc func openLink() {
		guard let urlString = user?.htmlUrl.absoluteString,
			let websiteUrl = URL(string: urlString) else { return }
		let safariVC = SFSafariViewController(url: websiteUrl)
		present(safariVC, animated: true, completion: nil)
	}

	@objc func openEmailApp() {
		guard let userEmail = user?.email,
			let validEmailURL = URL(string: "mailto:\(userEmail)") else { return }
		UIApplication.shared.open(validEmailURL)
	}

}

