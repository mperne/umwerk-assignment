//
//  ViewController.swift
//  umwerk-assignment
//
//  Created by Miha Perne on 22/04/2019.
//  Copyright © 2019 Miha Perne. All rights reserved.
//

import UIKit

class UserViewController: UIViewController {

	@IBOutlet private weak var tableView: UITableView!

	let usersViewModel = GitHubUsersViewModel()

	override func viewDidLoad() {
		super.viewDidLoad()
		
		tableView.dataSource = self
		tableView.delegate = self

		navigationController?.navigationBar.barTintColor = Constants.Colors.backgroundColor

		usersViewModel.usersUpdateCallback = { [weak self] newUsers in
//			guard let indexPathsToInsert = self?.calculateIndexPathsToInsert(from: newUsers) else { return }
//			self?.tableView.beginUpdates()
//			self?.tableView.insertRows(at: indexPathsToInsert, with: .none)
//			self?.tableView.endUpdates()
			self?.tableView.reloadData()
		}

		usersViewModel.usersResetCallback = { [weak self] in
			self?.tableView.reloadData()
		}

		usersViewModel.errorCallback = { error in
			print(error)
		}
	}

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

		guard let userDetailViewController = segue.destination as? UserDetailViewController,
			segue.identifier == Segue.userDetails.rawValue,
			let indexPath = tableView.indexPathForSelectedRow else { return }
		
		userDetailViewController.user = usersViewModel.gitHubUsers[indexPath.row]
	}

}

extension UserViewController: UITableViewDataSource, UITableViewDelegate {

//	private func calculateIndexPathsToInsert(from newUsers: [GitHubUser]) -> [IndexPath] {
//		let startIndex = usersViewModel.gitHubUsers.count - newUsers.count
//		let endIndex = startIndex + newUsers.count
//		return (startIndex..<endIndex).map { IndexPath(row: $0, section: 0) }
//	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return usersViewModel.gitHubUsers.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: UserTableViewCell.reuseIdentifier, for: indexPath) as! UserTableViewCell
		cell.user = usersViewModel.gitHubUsers[indexPath.row]
		return cell
	}

	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		if indexPath.row > usersViewModel.gitHubUsers.count - 6 {
			usersViewModel.fetchNextBatch()
		}
	}
}
