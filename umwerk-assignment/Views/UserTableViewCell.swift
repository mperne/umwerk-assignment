//
//  UserTableViewCell.swift
//  umwerk-assignment
//
//  Created by Miha Perne on 22/04/2019.
//  Copyright © 2019 Miha Perne. All rights reserved.
//

import Foundation
import UIKit
import Nuke

protocol ReuseIdentifying {
	static var reuseIdentifier: String { get }
}

extension ReuseIdentifying {
	static var reuseIdentifier: String {
		return String(describing: Self.self)
	}
}

class UserTableViewCell: UITableViewCell, ReuseIdentifying {
	
	@IBOutlet private weak var userImageView: UIImageView!
	@IBOutlet private weak var userNameLabel: UILabel!
	@IBOutlet private weak var userIDLabel: UILabel!

	var user: GitHubUser? {
		didSet {

			guard let user = user else {
				return
			}

			userNameLabel.text = user.login
			userIDLabel.text = "id: " + String(user.id)
			Nuke.loadImage(with: user.avatarUrl, into: userImageView)

		}
	}

	override func awakeFromNib() {
		super.awakeFromNib()
		userImageView.layer.cornerRadius = userImageView.frame.height / 2
	}

	override func prepareForReuse() {
		user = nil
		userImageView.image = nil
		userNameLabel.text = nil
		userIDLabel.text = nil
	}
}
