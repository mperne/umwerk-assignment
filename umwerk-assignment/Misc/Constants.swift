//
//  Constants.swift
//  umwerk-assignment
//
//  Created by Miha Perne on 22/04/2019.
//  Copyright © 2019 Miha Perne. All rights reserved.
//

import Foundation
import UIKit

enum ColorScheme {
	case dark
	case lightBlue
}

struct Constants {
	struct Network {
		static let scheme = "https"
		static let host = "api.github.com"
		static let usersPath = "/search/users"
		static let userPath = "/users"
	}

	struct Pagination {
		static let itemsPerPage = 10
	}

	struct Colors {
		static var backgroundColor: UIColor {
			#if DEBUGBLACK
				return UIColor(red: 57 / 255, green: 62 / 255, blue: 70 / 255, alpha: 1.0)
			#elseif DEBUGBLUE
				return UIColor(red: 162 / 255, green: 168 / 255, blue: 211 / 255, alpha: 1.0)
			#endif
			return .white
		}
	}
}
