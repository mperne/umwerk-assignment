//
//  Segue.swift
//  umwerk-assignment
//
//  Created by Miha Perne on 22/04/2019.
//  Copyright © 2019 Miha Perne. All rights reserved.
//

import Foundation

enum Segue: String {
	case userDetails = "userDetailSegue"
}
