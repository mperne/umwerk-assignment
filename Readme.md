# Umwerk assignment

## Specifications

1. Create a new project (use Swift- preferred)
 
2.  Setup 2 Xcode targets, where one sets the application background color to black and the other to blue.
 
3. When opening the app, show a list of all Java developers.
 
    Use the Github API to get the users
 
    Load maximum 10 users at once (paging!).
    Each user item should contain at least:
 
    username
    avatar
    id
 
4. Show a more detailed screen of the user, when you click on a user item.
 
    Feel free to add any details you want.
    Mandatory details are the email address (if the user has one) , github website and number of followers.
   Position the number of followers on bottom left on tablets on on bottom right on phones.
    When clicking on the email address, open the default email app on the device.
    When clicking on the githib website adress, open the website inside the app.
5. Create today app extension (widget) that displays the name of random github user that you loaded before (in step 3).
 
### Bonus: 
   Upload your code to a repository and share the code with us so we can do a review (bitbucket, github, ...)
 
## Setup

	carthage bootstrap --use-submodules --use-ssh --platform ios

   Change **Bundle Identifier** and ** Team** for each target.
